# Hashes son arreglos asociativos 
# y se pueden indexar con string, numero simbolo etc ...
#              Llave    valor
lenguajes = {'Python'=>'Flask', 'Ruby'=>'Ruby on Rails', 'Php'=>'CodeIgniter'}

lenguajes = {1 =>'Flask', 2 =>'Ruby on Rails', 3 =>'CodeIgniter'}

lenguajes = {:f =>'Flask', :r =>'Ruby on Rails', :c =>'CodeIgniter'}

lenguajes = {:a =>'Flask', 4 =>'Ruby on Rails', 'Php' =>'CodeIgniter'}

# Accedemos al hash atraves de su llave la llave que puede ser 
# un string, numero simbolo

# puts lenguajes[2]
# puts lenguajes[3]
# puts lenguajes[1]

puts lenguajes[:c]
puts lenguajes[:f]
puts lenguajes[:r]
# puts lenguajes.length

