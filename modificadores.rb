# Modificadores de acceso
# public : publico
# private : privado
# protected : protegido

# Por defecto los metodos de una clase son publicos
class Vehiculo
    # Constructor de la clase
    def initialize(marca, color)
        @marca = marca
        @color = color
    end

    def arranque
        puts "El #{@marca} de color #{@color} se ha encendido"
    end

    # Se define asi
    #  ↓
    private 
    def apagar
        puts "El #{@marca} de color #{@color} se ha apagado"
    end

    # O se definen asi
    private :marca, :color

    attr_accessor :marca, :color
    
end

class Auto < Vehiculo
# Esta clase hereda de la clase Vehiculo
# y por lo tanto la podemos instanciar para acceder
# a todos los metodos de la clase Vehiculo
end

class Moto < Vehiculo
    # Sobre escritura de métodos es volver a crear el metodo con diferente propiedad
    def arranque
        puts "La #{@marca} de color #{@color} se ha encendido"
    end
end

coche = Auto.new('Audi', 'Verde')
moto =  Moto.new('Honda', 'Roja')
puts " #{moto.arranque}"
puts " #{moto.apagar}"