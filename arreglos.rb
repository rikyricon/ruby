# Arreglo es una variable con posiciones y acepta a su ves variables ya definidas
# Todo lenguaje empieza a contar desde 0
         # 0  1  2  3   4
         # ↓  ↓  ↓  ↓   ↓
numeros = [1, 2, 3, 4, true] # arreglo de numeros

cadenas = ['Libia', 'España', 'Ecuador', 'Ruanda'] # arreglo de cadenas o string

puts numeros[4] # <-- con corchetes y el numero accedemos a la posicion de cada elemento del arreglo
puts cadenas[3]

# Arreglos con llenado %w reconoe como elementos del arreglo

cadena = %w{España Ecuador Ruanda}
puts cadena[0]